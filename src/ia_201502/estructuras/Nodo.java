/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia_201502.estructuras;

import java.util.Comparator;

/**
 *
 * @author jamil
 */
public class Nodo implements Comparator<Nodo> {
    
    private int[][] estado;
    private int heuristica;

    public Nodo() {
    }

    public Nodo(int[][] estado, int heuristica) {
        this.estado = estado;
        this.heuristica = heuristica;
    }       

    public int[][] getEstado() {
        return estado;
    }

    public void setEstado(int[][] estado) {
        this.estado = estado;
    }

    public int getHeuristica() {
        return heuristica;
    }

    public void setHeuristica(int heuristica) {
        this.heuristica = heuristica;
    }

    //Sirve para poder hacer sort al objeto a base de sus heuristicas!
    @Override
    public int compare(Nodo t, Nodo t1) {
        return t.getHeuristica() - t1.getHeuristica();
    } 
}
