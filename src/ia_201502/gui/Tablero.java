/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia_201502.gui;

import ia_201502.puzzle.Puzzle;
import java.awt.Color;
import java.util.List;
import javax.swing.JLabel;

/**
 *
 * @author jamil
 */
public class Tablero {
    
    String[][] matriz = new String[3][3];

    public void graficarTableroGanador(List<JLabel> lista) {
        String[] palabras = {"�", "G", "A", "N", "A", "S", "T", "E", "!"};
        int k = 0;
        for (k = 0; k < 9; k++) {
            lista.get(k).setText(palabras[k]);
            lista.get(k).setForeground(Color.GREEN);
        }
    }
    
    public void graficarTableroPerdedor(List<JLabel> lista) {
        String[] palabras = {"P", "E", "R", "D", "I", "S", "T", "E", "!"};
        int k = 0;
        for (k = 0; k < 9; k++) {
            lista.get(k).setText(palabras[k]);
            lista.get(k).setForeground(Color.RED);
        }
    }
    
    public void graficarTableroTramposo(List<JLabel> lista) {
        String[] palabras = {"T", "R", "A", "M", "P", "O", "S", "O", "!"};
        int k = 0;
        for (k = 0; k < 9; k++) {
            lista.get(k).setText(palabras[k]);
            lista.get(k).setForeground(Color.RED);
        }
    }
    

    public void graficarTablero(List<JLabel> lista) {
        int i, j;
        int k = 0;
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                lista.get(k).setText(matriz[i][j]);

                if (matriz[i][j].compareTo("0") == 0) {
                    lista.get(k).setForeground(Color.RED);
                } else {
                    lista.get(k).setForeground(Color.BLACK);
                }
                k++;
            }
        }
    }

    public boolean cumpleObjetivo(Puzzle puzzle, int[][] objetivo) {

        if (puzzle.haGanado(objetivo)) {
            return true;

        }
        return false;
    }

    public String[][] getMatriz() {
        return matriz;
    }

    public void setMatriz(String[][] matriz) {
        this.matriz = matriz;
    }
}
