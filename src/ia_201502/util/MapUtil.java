/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia_201502.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author jamil
 */
public class MapUtil {
    
    public static List<List<Integer> > listaHaciaPadre(int[][] hijo, int[][] padre, HashMap<List<Integer>, List<Integer> > padres) { 
        List<Integer> origen = MatrizUtil.aplanar(hijo);
        List<Integer> destino = MatrizUtil.aplanar(padre);
        
        if (padres.getOrDefault(origen, null) == null) {
            return null;
        }
        List<Integer> estadoActual = origen;
        List<List<Integer>> camino = new ArrayList<>();
        camino.add(estadoActual);
        while (!estadoActual.equals(destino) && estadoActual.get(0) != -1) {
            //Obtener padre
            estadoActual = padres.get(estadoActual);
            camino.add(estadoActual);
        }
        return camino;
    }

    public static int distanciaHastaPadre(int[][] hijo, int[][] padre, HashMap<List<Integer>, List<Integer> > padres) {
        List<Integer> origen = MatrizUtil.aplanar(hijo);
        List<Integer> destino = MatrizUtil.aplanar(padre);
        
        if (padres.getOrDefault(origen,null) == null) {
            return -1;
        }
        List<Integer> estadoActual = origen;
        int distancia = 0;
        while (!estadoActual.equals(destino) && estadoActual.get(0) != -1) {
            distancia++;
            //Obtener padre
            estadoActual = padres.get(estadoActual);
        }
        return distancia;
    }
    
}
