/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia_201502.gui;

/**
 *
 * @author jamil
 */

//Esta interfaz sirve para clases hijas puedan acceder al cambio de carta del padre.
public interface Carta {    
    void show(String carta); 
}
