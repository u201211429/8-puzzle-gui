/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia_201502.puzzle;

import ia_201502.estructuras.Reporte;
import ia_201502.impresoras.Impresor;
import ia_201502.algoritmos.Algoritmo;
import java.util.List;

/**
 *
 * @author jamil
 */
public class PuzzleAI {
    
    Puzzle puzzle;
    int[][] estadoFinal;
    Algoritmo algoritmo;
    Impresor impresor;
    Reporte reporte;
    
    public PuzzleAI() {
    }

    public PuzzleAI(Puzzle puzzle, int[][] estadoFinal, Algoritmo algoritmo, Impresor impresor) {
        this.puzzle = puzzle;
        this.estadoFinal = estadoFinal;
        this.algoritmo = algoritmo;
        this.impresor = impresor;
    }
    
    public PuzzleAI(Puzzle puzzle, int[][] estadoFinal, Algoritmo algoritmo) {
        this.puzzle = puzzle;
        this.estadoFinal = estadoFinal;
        this.algoritmo = algoritmo;
    }
 
    
    public void resolver() {
        algoritmo.correr();
        impresor.reportar(algoritmo.getReporte());               
    }
    
    public List<Integer> getSiguienteMovimiento() {
        algoritmo.correr();
        int size = algoritmo.getReporte().getResultado().size();
        return algoritmo.getReporte().getResultado().get(size-2);        
    }
}
